export declare class BasicFixedComponent {
    rows: any[];
    columns: ({
        prop: string;
    } | {
        name: string;
    })[];
    constructor();
    fetch(cb: any): void;
}
