export declare class DarkThemeComponent {
    rows: any[];
    loadingIndicator: boolean;
    reorderable: boolean;
    columns: ({
        prop: string;
    } | {
        name: string;
    })[];
    constructor();
    fetch(cb: any): void;
}
