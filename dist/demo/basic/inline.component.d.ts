export declare class InlineEditComponent {
    editing: {};
    rows: any[];
    constructor();
    fetch(cb: any): void;
    updateValue(event: any, cell: any, rowIndex: any): void;
}
