export declare class MultipleTablesComponent {
    columns1: ({
        prop: string;
    } | {
        name: string;
    })[];
    columns2: ({
        prop: string;
        Name: string;
    } | {
        name: string;
    })[];
    rows1: {
        name: string;
        gender: string;
        company: string;
    }[];
    rows2: {
        name: string;
        gender: string;
    }[];
}
