export declare class RowDetailsComponent {
    table: any;
    rows: any[];
    expanded: any;
    timeout: any;
    constructor();
    onPage(event: any): void;
    fetch(cb: any): void;
    toggleExpandRow(row: any): void;
    onDetailToggle(event: any): void;
}
