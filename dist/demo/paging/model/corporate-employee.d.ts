/**
 * A model for an individual corporate employee
 */
export declare class CorporateEmployee {
    name: string;
    gender: string;
    company: string;
    age: number;
    constructor(name: string, gender: string, company: string, age: number);
}
