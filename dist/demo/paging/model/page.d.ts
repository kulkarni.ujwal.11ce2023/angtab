/**
 * An object used to get page information from the server
 */
export declare class Page {
    size: number;
    totalElements: number;
    totalPages: number;
    pageNumber: number;
}
