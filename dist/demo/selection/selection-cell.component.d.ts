export declare class CellSelectionComponent {
    rows: any[];
    selected: any[];
    columns: any[];
    constructor();
    fetch(cb: any): void;
    onSelect(event: any): void;
    onActivate(event: any): void;
}
